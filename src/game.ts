import { HttpException } from '@nestjs/common';

export type Player = 'X' | 'O';

export type CellIndex = {
  row: number;
  col: number;
};

export interface Cell extends CellIndex {
  player?: Player;
}

export type Grid = Row[];
export type Row = Col[];
export type Col = Cell;

export class Game {
  currentPlayer: Player = 'X';
  players: Player[] = ['X', 'O'];

  // cols -> cell -> Player
  grid: Grid;

  constructor() {
    this.grid = [];
    for (let row = 0; row < 3; row++) {
      const cells: Cell[] = [];
      for (let col = 0; col < 3; col++) {
        const cell: Cell = { row, col };
        cells.push(cell);
      }
      this.grid.push(cells);
    }
  }

  place(index: CellIndex) {
    const cell = this.grid[index.row][index.col];
    if (cell.player)
      throw new HttpException('the cell is already occupied', 400);
    cell.player = this.currentPlayer;
    this.nextPlayer();
  }

  private nextPlayer() {
    let idx = this.players.indexOf(this.currentPlayer);
    idx = (idx + 1) % this.players.length;
    this.currentPlayer = this.players[idx];
  }

  reset() {
    Object.assign(this, new Game());
  }
}
