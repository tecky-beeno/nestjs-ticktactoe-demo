import { Injectable } from '@nestjs/common';
import { Cell, Game } from '../game';

@Injectable()
export class GameService {
  private currentGame: Game;

  constructor() {
    this.currentGame = new Game();
  }

  getCurrentGame() {
    return this.currentGame;
  }

  place(cell: Cell) {
    this.currentGame.place(cell);
    return this.currentGame;
  }

  reset() {
    this.currentGame.reset();
    return this.currentGame;
  }
}
