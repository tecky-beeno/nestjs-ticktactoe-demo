import { Body, Controller, Get, HttpException, Post } from '@nestjs/common';
import { CellIndex, Game } from '../game';
import { GameService } from './game.service';

@Controller('game')
export class GameController {
  constructor(public gameService: GameService) {}

  @Get()
  async getGame(): Promise<Game> {
    return this.gameService.getCurrentGame();
  }

  @Post('/place')
  async place(@Body('index') index: CellIndex): Promise<Game> {
    if (typeof index !== 'object' || !index) {
      throw new HttpException(
        "missing json object 'index' in request body",
        400,
      );
    }
    if (typeof index.row !== 'number') {
      throw new HttpException("missing numeric 'row' in index", 400);
    }
    if (typeof index.col !== 'number') {
      throw new HttpException("missing numeric 'col' in index", 400);
    }
    return this.gameService.place(index);
  }

  @Post('/reset')
  async reset(): Promise<Game> {
    return this.gameService.reset();
  }
}
